﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using TZATranslations;
using TZATranslations.StringFiles;

namespace EncodeNPCDic
{
    class EncodeNPCDic
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vaan's NPC Dictionary Encoder");

            Environment.CurrentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            try
            {
                if (args.Length != 1)
                {
                    Console.WriteLine("Please use as 'DecodeNPCDic.exe <filepathhere>',\nor drag the txt file you wish" +
                        " to convert into the executable.");
                    Console.Read();
                    return;
                }
                Encode(args[0]);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.ReadKey();
            }
        }

        public static void Encode(string file)
        {
            var studio = new Studio(GetDecoder());
            if (!File.Exists(file)) return;
            if (Path.GetExtension(file) == ".bin") return;
            var original = Path.GetDirectoryName(file) + "\\" + Path.GetFileNameWithoutExtension(file) + ".bin";
            if (!File.Exists(original)) return;

            var lines = File.ReadAllText(file).Split('\n');
            var bytes = File.ReadAllBytes(original);
            var fileObj = new NPCDic(bytes);
            var output = studio.EncodeBin(fileObj, lines);

            File.WriteAllBytes(original + ".pack", output.ToArray());
        }

        public static Dictionary<byte, char> GetDecoder()
        {
            var lines = File.ReadAllLines("encoding.csv", Encoding.GetEncoding("iso-8859-1"));
            var dict = new Dictionary<byte, char>();

            foreach (var line in lines)
            {
                var frags = line.Split(',');
                char value;
                switch (frags[0])
                {
                    case "COMMA": value = ','; break;
                    case "\u008c": value = 'Œ'; break;
                    case "\u009c": value = 'œ'; break;
                    default: value = frags[0][0]; break;
                }
                byte key = CallTryParse(frags[1], NumberStyles.HexNumber);
                dict.Add(key, value);
            }

            return dict;
        }

        private static byte CallTryParse(string stringToConvert, NumberStyles styles = NumberStyles.HexNumber)
        {
            Byte byteValue;
            bool result = Byte.TryParse(stringToConvert, styles,
                                        null as IFormatProvider, out byteValue);
            return byteValue;
        }
    }
}
