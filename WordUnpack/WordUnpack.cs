﻿using System;
using System.Collections.Generic;
using System.IO;
using TZATranslations;

namespace WordUnpack
{
    class WordUnpack
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vaan's Word Unpacker!");

            Environment.CurrentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            try
            {
                if (args.Length != 1)
                {
                    Console.WriteLine("Please use as 'WordUnpack.exe <filepathhere>',\nor drag the bin file you wish" +
                        " to unpack into singular bin files.");
                    Console.Read();
                    return;
                }

                if (!File.Exists(args[0]))
                {
                    Console.WriteLine("The file does not exist.");
                    Console.Read();
                    return;
                }
                var folder = Path.GetDirectoryName(args[0]);
                var file = Path.GetFileNameWithoutExtension(args[0]);
                if (Directory.Exists(folder + '\\' + file))
                {
                    Console.WriteLine("Destination folder already exists.");
                    Console.Read();
                    return;
                }

                Unpack(args[0], folder + '\\' + file);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.ReadKey();
            }
        }

        public static void Unpack(string file, string destFolder)
        {
            var bytes = File.ReadAllBytes(file);
            var word = new TZATranslations.PackFiles.WordPack(bytes);

            Directory.CreateDirectory(destFolder);

            for (int processed = 0; processed < word.SubFileCount; processed++)
            {
                File.WriteAllBytes(destFolder + "\\word" + processed.ToString("D2") + ".bin", word.GetSubFile(processed));
            }
        }
    }
}
