﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ListFileSizes
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach(var file in args)
            {
                if (!File.Exists(file)) continue;

                var info = new FileInfo(file);
                Console.WriteLine(info.Name + " " + info.Length);
            }
            Console.Write("Done"); Console.ReadKey();
        }
    }
}
