﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

namespace BattleUnPacker
{
    class BattleUnPacker
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vaan's BattlePack Unpacker!");

            Environment.CurrentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            try
            {
                if (args.Length != 1)
                {
                    Console.WriteLine("Please use as 'BattleUnPacker.exe <filepathhere>',\nor drag the bin file you wish" +
                        " to unpack into singular bin files.");
                    Console.Read();
                    return;
                }

                if(!File.Exists(args[0]))
                {
                    Console.WriteLine("The file does not exist.");
                    Console.Read();
                    return;
                }
                var folder = Path.GetDirectoryName(args[0]);
                var file = Path.GetFileNameWithoutExtension(args[0]);
                if(Directory.Exists(folder + '\\' + file))
                {
                    Console.WriteLine("Destination folder already exists.");
                    Console.Read();
                    return;
                }

                Unpack(args[0], folder + '\\' + file);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.ReadKey();
            }
        }

        public static void Unpack(string file, string destFolder)
        {
            var pack = new TZATranslations.PackFiles.BattlePack(File.ReadAllBytes(file));

            if (pack.SubFileCount == 0)
                throw new Exception("Error loading subfiles.");

            Directory.CreateDirectory(destFolder);
            for(var i = 0; i < pack.SubFileCount; i++)
            {
                File.WriteAllBytes(destFolder+"\\pack" + i.ToString("D2") + ".bin", pack.GetSubFile(i));
            }
        }
    }
}
