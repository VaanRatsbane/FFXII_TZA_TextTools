﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TZATranslations;

namespace WordDecoder
{
    //ignore 2 and 7
    class WordDecoder
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vaan's Word Decoder");
            Environment.CurrentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            try
            {
                if (args.Length < 1)
                {
                    Console.WriteLine("Please use as 'WordDecoder.exe <filepathshere>',\nor drag the bin file you wish" +
                        " to convert into the executable.");
                    Console.Read();
                    return;
                }
                Decode(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.ReadKey();
            }
        }

        public static void Decode(string[] files)
        {
            Dictionary<byte, char> decoder = GetDecoder();
            var studio = new Studio(decoder);

            foreach (var file in files)
            {
                if (!File.Exists(file)) continue;
                if (Path.GetExtension(file) == ".txt") return;

                var bytes = File.ReadAllBytes(file);
                var fileObj = new TZATranslations.StringFiles.WordFile(bytes);
                var lines = studio.DecodeFile(fileObj);
                File.WriteAllText(file.Substring(0, file.Length - Path.GetExtension(file).Length) + ".txt", String.Join("\n", lines));
            }
        }

        public static Dictionary<byte, char> GetDecoder()
        {
            var lines = File.ReadAllLines("encoding.csv", Encoding.GetEncoding("iso-8859-1"));
            var dict = new Dictionary<byte, char>();

            foreach (var line in lines)
            {
                var frags = line.Split(',');
                char value;
                switch (frags[0])
                {
                    case "COMMA": value = ','; break;
                    case "\u008c": value = 'Œ'; break;
                    case "\u009c": value = 'œ'; break;
                    default: value = frags[0][0]; break;
                }
                byte key = CallTryParse(frags[1], NumberStyles.HexNumber);
                dict.Add(key, value);
            }

            return dict;
        }

        private static byte CallTryParse(string stringToConvert, NumberStyles styles = NumberStyles.HexNumber)
        {
            Byte byteValue;
            bool result = Byte.TryParse(stringToConvert, styles,
                                        null as IFormatProvider, out byteValue);
            return byteValue;
        }
    }
}
