﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TZATranslations;

namespace BattleRePacker
{
    class BattleRePacker
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vaan's BattlePack Repacker!");

            Environment.CurrentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            try
            {
                if (args.Length != 1)
                {
                    Console.WriteLine("Please use as 'BattleUnPacker.exe <folderpathhere>',\nor drag the folder that contains the battlepack fragments you wish" +
                        " to pack into a single bin file.");
                    Console.Read();
                    return;
                }

                if (!Directory.Exists(args[0]))
                {
                    Console.WriteLine("The folder does not exist.");
                    Console.Read();
                    return;
                }

                if (!File.Exists(args[0]+".bin"))
                {
                    Console.WriteLine("The original file does not exist.");
                    Console.Read();
                    return;
                }

                var folder = Path.GetDirectoryName(args[0]);
                var file = Path.GetFileNameWithoutExtension(args[0]);
                if (Directory.Exists(folder + ".bin.pack"))
                {
                    Console.WriteLine("Destination file already exists.");
                    Console.Read();
                    return;
                }

                Repack(args[0], folder + ".bin");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.ReadKey();
            }
        }

        public static void Repack(string folder, string destFile)
        {
            var pack = new TZATranslations.PackFiles.BattlePack(File.ReadAllBytes(folder + ".bin"));
            var files = Directory.EnumerateFiles(folder).OrderBy(c => c).ToList();
            if (files.Count != pack.SubFileCount)
                throw new Exception("Invalid amount of files being repacked.");
            for (int i = 0; i < files.Count; i++)
            {
                pack.UpdateSubFile(i, File.ReadAllBytes(files[i]));
            }
            var newBytes = pack.Repack();

            File.WriteAllBytes(folder + ".bin.pack", newBytes.ToArray());
        }
    }
}
