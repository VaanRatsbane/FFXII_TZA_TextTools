﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations
{
    public class Helpers
    {
        public static byte CallTryParse(string stringToConvert, NumberStyles styles = NumberStyles.HexNumber)
        {
            Byte byteValue;
            bool result = Byte.TryParse(stringToConvert, styles,
                                        null as IFormatProvider, out byteValue);
            return byteValue;
        }
    }
}
