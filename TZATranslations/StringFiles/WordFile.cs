﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.StringFiles
{
    /// <summary>
    /// WordFile. Also used for Battle-pack's subfile 38.
    /// </summary>
    public class WordFile : IStringFile
    {

        byte[] originalBytes;
        int blockStart;
        int stringCount;
        int stringsStart;

        public WordFile(byte[] FileBytes)
        {
            originalBytes = FileBytes;
            blockStart = 0x04;
            stringCount = BitConverter.ToInt32(originalBytes, 0x00);
            stringsStart = blockStart + (stringCount + 1) * 4;
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var positions = new List<int>();
            for (int i = blockStart; i < stringsStart; i += 4)
            {
                var position = BitConverter.ToInt32(originalBytes, i);

                if (!positions.Contains(position))
                    positions.Add(position);
            }

            var output = String.Empty;
            for (int i = 0; i < positions.Count - 1; i++)
            {
                var start = positions[i];
                var end = positions[i + 1];
                var line = String.Empty;
                for (int p = start; p < end; p++)
                {
                    var bt = originalBytes[p];
                    if (Decoder.ContainsKey(bt))
                        line += Decoder[bt];
                    else
                        line += "{" + bt.ToString("X2") + "}";
                }
                output += line + '\n';
            }

            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] NewLines)
        {
            var output = new List<byte>();

            for (int i = 0; i < stringsStart; i++) //Copy header, string count and string offsets
                output.Add(originalBytes[i]);

            int stringPos = stringsStart;
            for (int i = 0; i < stringCount; i++)
            {
                var line = NewLines[i];
                int j = 0;
                while (j < line.Length)
                {
                    var c = line[j];
                    if (Encoder.ContainsKey(c))
                    {
                        stringPos++;
                        output.Add(Encoder[c]);
                    }
                    else
                    {
                        if (c == '{')
                        {
                            var hex = line[j + 1] + "" + line[j + 2];
                            if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + i + ", pos " + j);
                            j += 3;
                            output.Add(Helpers.CallTryParse(hex));
                            stringPos++;
                        }
                        else throw new InvalidDataException("Invalid char in line " + i + " at position " + j);
                    }
                    j++;
                }

                var newOffset = stringPos;

                if (i < stringCount)
                {
                    var bts = BitConverter.GetBytes(newOffset);
                    output[blockStart + (i + 1) * 4] = bts[0];
                    output[blockStart + (i + 1) * 4 + 1] = bts[1];
                    output[blockStart + (i + 1) * 4 + 2] = bts[2];
                    output[blockStart + (i + 1) * 4 + 3] = bts[3];
                }
            }

            return output;
        }
    }
}
