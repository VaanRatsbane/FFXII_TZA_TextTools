﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.StringFiles
{
    /// <summary>
    /// To deal with the file menumap_guide00.bin
    /// </summary>
    public class MenuMapGuide : IStringFile
    {
        byte[] originalBytes;

        public MenuMapGuide(byte[] FileBytes)
        {
            originalBytes = FileBytes;
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var output = String.Empty;
            var stringStart = 0x278;
            for (int i = 0x14; i < stringStart; i += 8)
            {
                var offset = BitConverter.ToInt32(originalBytes, i) + stringStart;
                var line = String.Empty;
                var limit = i + 8 < stringStart ? stringStart + BitConverter.ToInt32(originalBytes, i + 8) : originalBytes.Length - 2;
                for (int j = offset; j < limit; j++)
                {
                    var bt = originalBytes[j];
                    if (Decoder.ContainsKey(bt))
                        line += Decoder[bt];
                    else
                        line += "{" + bt.ToString("X2") + "}";
                }
                output += line.Substring(0, line.Length - 4) + '\n';
            }
            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] NewLines)
        {
            throw new NotImplementedException();
        }
    }
}
