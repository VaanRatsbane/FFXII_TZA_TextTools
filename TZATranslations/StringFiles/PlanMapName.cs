﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TZATranslations.StringFiles
{
    public class PlanMapName : IStringFile
    {
        byte[] originalBytes;
        List<int> offsetPositions;

        public PlanMapName(byte[] FileBytes)
        {
            originalBytes = FileBytes;
            offsetPositions = new List<int>();
            for(int i = 0x70; i < 0xAC6; i+=2)
            {
                if (BitConverter.ToInt16(originalBytes, i) == 0) continue;
                offsetPositions.Add(i);
            }
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var readOffsets = new List<int>();
            var output = String.Empty;
            foreach (var offsetPos in offsetPositions)
            {
                var offset = BitConverter.ToInt16(originalBytes, offsetPos);
                if (readOffsets.Contains(offset)) continue;
                readOffsets.Add(offset);
                var line = String.Empty;
                byte bt;
                do
                {
                    bt = originalBytes[offset++];
                    if (Decoder.ContainsKey(bt))
                        line += Decoder[bt];
                    else
                        line += "{" + bt.ToString("X2") + "}";
                } while (bt != 0);
                output += line.Substring(0, line.Length - 4) + '\n';
            }
            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] NewLines)
        {
            var offsets = offsetPositions.Select(c => BitConverter.ToInt16(originalBytes, c)).ToList();
            var uniqueOffsets = offsets.Distinct().ToList();

            if (uniqueOffsets.Count != NewLines.Length)
                throw new Exception("Line count does not match original's.");

            var output = new List<byte>();
            var oldToNewOffsets = new Dictionary<int, int>();

            for (int i = 0; i < 0xAC6; i++)
                output.Add(originalBytes[i]);

            var orderedOffsets = uniqueOffsets.OrderBy(c => c);
            foreach(var order in orderedOffsets)
            {
                var index = uniqueOffsets.IndexOf(order);

                oldToNewOffsets.Add(order, output.Count);

                var line = NewLines[index];
                int j = 0;
                while (j < line.Length)
                {
                    var c = line[j];
                    if (Encoder.ContainsKey(c))
                    {
                        output.Add(Encoder[c]);
                    }
                    else
                    {
                        if (c == '{')
                        {
                            var hex = line[j + 1] + "" + line[j + 2];
                            if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + index + ", pos " + j);
                            j += 3;
                            output.Add(Helpers.CallTryParse(hex));
                        }
                        else throw new InvalidDataException("Invalid char in line " + index + " at position " + j);
                    }
                    j++;
                }
                output.Add(0x00);
            }
            output.Add(0x00);
            output.Add(0x00);
            output.Add(0x00);

            for(int i = 0x70; i < 0xAC6; i+=2)
            {
                var oldOffset = BitConverter.ToInt16(originalBytes, i);
                if(oldOffset != 0)
                {
                    var newOffset = oldToNewOffsets[oldOffset];
                    var newBytes = BitConverter.GetBytes(newOffset);
                    output[i] = newBytes[0];
                    output[i + 1] = newBytes[1];
                }
            }

            return output;
        }
    }
}
