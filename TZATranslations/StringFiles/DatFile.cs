﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.StringFiles
{
    /// <summary>
    /// To deal with the .dat files in myoshiok/handbook, specifically monster000 - monster003 and tutorial000
    /// </summary>
    public class DatFile : IStringFile
    {
        byte[] originalBytes;
        short nrOfBlocks;
        int startOfStrings;
        List<int> stringPosOffsets;
        //0x0C is start of TIM2, if 0xFFFF then it doesn't contain it
        int originalTIM2Offset;

        public DatFile(byte[] FileBytes)
        {
            originalBytes = FileBytes;

            nrOfBlocks = BitConverter.ToInt16(originalBytes, 0xA);
            originalTIM2Offset = BitConverter.ToInt16(originalBytes, 0xC);
            startOfStrings = BitConverter.ToInt16(originalBytes, 0x10) + 0x04;

            var blockOffsets = new short[nrOfBlocks];
            for (int i = 0; i < nrOfBlocks; i++)
                blockOffsets[i] = BitConverter.ToInt16(originalBytes, 0x12 + i * 0x02);

            stringPosOffsets = new List<int>();
            var blockTerminator = 72057591890477056;
            foreach (var offset in blockOffsets)
                for (int pos = offset + 0x04; BitConverter.ToInt64(originalBytes, pos) != blockTerminator; pos += 4)
                    stringPosOffsets.Add(pos);
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var positions = new List<int>();
            var output = String.Empty;
            for(int i = 0; i < stringPosOffsets.Count; i++)
            {
                var stringPos = startOfStrings + BitConverter.ToInt32(originalBytes, stringPosOffsets[i]);
                var end = (i < stringPosOffsets.Count - 1
                            ? startOfStrings + BitConverter.ToInt32(originalBytes, stringPosOffsets[i + 1]) - 0x04 : originalBytes.Length);
                byte bt;
                var line = String.Empty;
                do
                {
                    bt = originalBytes[stringPos++];
                    if (Decoder.ContainsKey(bt))
                        line += Decoder[bt];
                    else
                        line += "{" + bt.ToString("X2") + "}";
                } while (stringPos < end);
                output += line.Substring(0, line.Length - 4) + '\n';
            }
            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] NewLines)
        {
            if (stringPosOffsets.Count != NewLines.Length)
                throw new Exception("Line count doesn't match original's.");

            var output = new List<byte>();
            for (int i = 0; i < startOfStrings - 4; i++)
                output.Add(originalBytes[i]);

            var seperator = new byte[4] { 0x04, 0x00, 0x00, 0x00 };
            for(int l = 0; l < NewLines.Length; l++)
            {
                var newOffset = output.Count - startOfStrings + 4;
                var posOffset = stringPosOffsets[l];
                var bts = BitConverter.GetBytes(newOffset);
                output[posOffset] = bts[0];
                output[posOffset + 1] = bts[1];
                output[posOffset + 2] = bts[2];
                output[posOffset + 3] = bts[3];

                output.AddRange(seperator);
                var line = NewLines[l];
                int j = 0;
                while (j < line.Length)
                {
                    var c = line[j];
                    if (Encoder.ContainsKey(c))
                    {
                        output.Add(Encoder[c]);
                    }
                    else
                    {
                        if (c == '{')
                        {
                            var hex = line[j + 1] + "" + line[j + 2];
                            if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + l + ", pos " + j);
                            j += 3;
                            output.Add(Helpers.CallTryParse(hex));
                        }
                        else throw new InvalidDataException("Invalid char in line " + l + " at position " + j);
                    }
                    j++;
                }
                output.Add(0x00);
            }

            return output;
        }
    }
}
