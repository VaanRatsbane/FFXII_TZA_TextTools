﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.StringFiles
{
    /// <summary>
    /// To handle the menuquest.lst file in myoshiok/us/quest
    /// </summary>
    public class MenuQuestList : IStringFile
    {
        byte[] originalBytes;
        short nrOfStrings; //at 0xE, last two point to the final zero, thus length - 1
        int stringsStart; //at 0x8

        public MenuQuestList(byte[] FileBytes)
        {
            nrOfStrings = BitConverter.ToInt16(FileBytes, 0xE);
            stringsStart = BitConverter.ToInt32(FileBytes, 0x8);
            originalBytes = FileBytes;
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var positions = new List<int>();
            for(int i = 0; i < nrOfStrings - 2; i++) //only read lines
            {
                var position = stringsStart + BitConverter.ToInt16(originalBytes, 0x12 + i * 0x8);
                positions.Add(position);
            }

            var output = String.Empty;
            for (int i = 0; i < positions.Count; i++)
            {
                var pos = positions[i];
                var end = i + 1 < positions.Count ? positions[i + 1] : originalBytes.Length;
                if (pos == end) continue;
                var line = String.Empty;
                byte bt;
                do
                {
                    bt = originalBytes[pos++];
                    if (Decoder.ContainsKey(bt))
                        line += Decoder[bt];
                    else
                        line += "{" + bt.ToString("X2") + "}";
                } while (pos != end);
                output += line.Substring(0, line.Length - 4) + '\n';
            }

            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] NewLines)
        {
            if (NewLines.Length != nrOfStrings - 2)
                throw new Exception("Line count does not match original's.");

            var output = new List<byte>();
            for (int i = 0; i < stringsStart; i++)
                output.Add(originalBytes[i]);

            for(int i = 0; i < nrOfStrings - 2; i++)
            {
                var offsetPos = 0x12 + i * 0x8;
                var newOffset = output.Count - stringsStart;
                var bts = BitConverter.GetBytes(newOffset);
                output[offsetPos] = bts[0];
                output[offsetPos + 1] = bts[1];

                var line = NewLines[i];
                int j = 0;
                while (j < line.Length)
                {
                    var c = line[j];
                    if (Encoder.ContainsKey(c))
                    {
                        output.Add(Encoder[c]);
                    }
                    else
                    {
                        if (c == '{')
                        {
                            var hex = line[j + 1] + "" + line[j + 2];
                            if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + i + ", pos " + j);
                            j += 3;
                            output.Add(Helpers.CallTryParse(hex));
                        }
                        else throw new InvalidDataException("Invalid char in line " + i + " at position " + j);
                    }
                    j++;
                }
                output.Add(0x00);
            }

            var finalOffset = output.Count - stringsStart - 1; //point to final zero
            var byts = BitConverter.GetBytes(finalOffset);
            for(int i = nrOfStrings - 2; i < nrOfStrings; i++)
            {
                var offsetPos = 0x12 + i * 0x8;
                output[offsetPos] = byts[0];
                output[offsetPos + 1] = byts[1];
            }


            return output;
        }
    }
}