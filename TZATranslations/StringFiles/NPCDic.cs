﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.StringFiles
{
    /// <summary>
    /// NPC dictionary file. Contains all npc names.
    /// </summary>
    public class NPCDic : IStringFile
    {

        byte[] originalBytes;
        int blockStart;
        int stringCount;
        int stringsStart;

        public NPCDic(byte[] FileBytes)
        {
            originalBytes = FileBytes;
            blockStart = 0x0C;
            stringCount = BitConverter.ToInt32(originalBytes, 0x08);
            stringsStart = 0x0C + stringCount * 4;
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var output = String.Empty;
            int pos = 0;
            for (int i = 0; i < stringCount; i++)
            {
                var offset = BitConverter.ToInt32(originalBytes, blockStart + (i * 4));

                pos = offset;
                if (pos >= stringsStart)
                {
                    var line = String.Empty;
                    byte bt;
                    do
                    {
                        bt = originalBytes[pos++];
                        if (Decoder.ContainsKey(bt))
                            line += Decoder[bt];
                        else
                            line += "{" + bt.ToString("X2") + "}";
                    } while (bt != 0);

                    output += line.Substring(0, line.Length - 4) + '\n';
                }
            }
            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] lines)
        {
            var output = new List<byte>();

            for (int i = 0; i < stringsStart; i++) //Copy header, string count and string offsets
                output.Add(originalBytes[i]);

            int stringPos = BitConverter.ToInt32(originalBytes, 0x0C);
            for (int i = 0; i < stringCount; i++)
            {

                var line = lines[i];
                int j = 0;
                while (j < line.Length)
                {
                    var c = line[j];
                    if (Encoder.ContainsKey(c))
                    {
                        stringPos++;
                        output.Add(Encoder[c]);
                    }
                    else
                    {
                        if (c == '{')
                        {
                            var hex = line[j + 1] + "" + line[j + 2];
                            if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + i + ", pos " + j);
                            j += 3;
                            output.Add(Helpers.CallTryParse(hex));
                            stringPos++;
                        }
                        else throw new InvalidDataException("Invalid char in line " + i + " at position " + j);
                    }
                    j++;
                }
                output.Add(0x00);
                stringPos++;

                var newOffset = stringPos;

                if (i + 1 < stringCount) //offsets are only starts, so don't write an offset to the end of the last string
                {
                    var bts = BitConverter.GetBytes(newOffset);
                    output[blockStart + (i + 1) * 4] = bts[0];
                    output[blockStart + (i + 1) * 4 + 1] = bts[1];
                    output[blockStart + (i + 1) * 4 + 2] = bts[2];
                    output[blockStart + (i + 1) * 4 + 3] = bts[3];
                }
            }

            return output;
        }
    }
}
