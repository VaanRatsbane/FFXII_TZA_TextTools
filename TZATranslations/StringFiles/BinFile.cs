﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.StringFiles
{
    /// <summary>
    /// A standard bin file. Like the ones in testbattle/[language]/, with exception of battle_pack and word.
    /// </summary>
    public class BinFile : IStringFile
    {
        byte[] originalBytes;
        int blockStart;
        int posBlockStart { get { return blockStart + 16; } }
        int posAmount;
        int stringsStart { get { return blockStart + 16 + posAmount * 4; } }
        
        public BinFile(byte[] FileBytes)
        {
            blockStart = BitConverter.ToInt32(FileBytes, 0x14);
            posAmount = BitConverter.ToInt32(FileBytes, blockStart + 12) + 1;
            if (BitConverter.ToInt32(FileBytes, blockStart) != 0x01 || BitConverter.ToInt32(FileBytes, blockStart + 4) != 0x0C)
            {
                throw new InvalidDataException("Could not find block start at " + blockStart);
            }
            originalBytes = FileBytes;
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var output = String.Empty;
            
            var blockEnd = blockStart + BitConverter.ToInt32(originalBytes, blockStart + 8);

            var readOffsets = new List<int>();

            int pos = 0;
            for (int i = 0; i < posAmount - 1; i++)
            {
                var offset = BitConverter.ToInt32(originalBytes, blockStart + 0x10 + (i * 4));
                if (readOffsets.Contains(offset)) continue;

                int nextOffset;
                int k = i + 1;
                do
                {
                    nextOffset = BitConverter.ToInt32(originalBytes, blockStart + 0x10 + (k * 4));
                    k++;
                } while (nextOffset == offset || readOffsets.Contains(nextOffset)); //if this throws, something is seriously wrong

                readOffsets.Add(offset);
                pos = offset + blockStart + 12;
                var endPos = nextOffset + blockStart + 12;
                if (pos < blockEnd && pos >= stringsStart)
                {
                    var line = String.Empty;
                    byte bt;
                    do
                    {
                        bt = originalBytes[pos++];
                        if (Decoder.ContainsKey(bt))
                            line += Decoder[bt];
                        else
                            line += "{" + bt.ToString("X2") + "}";
                    } while (bt != 0 || (nextOffset > offset && pos < endPos));
                    output += line.Substring(0, line.Length - 4) + '\n';
                }
            }
            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] lines)
        {
            var output = new List<byte>();
            var stringBlockStart = posBlockStart + 4 * posAmount;
            //Add header, filling, block metadata and offsets
            for (int i = 0; i < stringBlockStart; i++)
                output.Add(originalBytes[i]);

            //Count ending 00 for file consistency, probably doesn't matter but just to be safe
            int finalOffset = blockStart + BitConverter.ToInt32(originalBytes, blockStart + 0x08);
            int endingZeros = originalBytes.Length - finalOffset;

            var oldToNewOffsets = new Dictionary<int, int>();

            int selectedLine = 0;

            oldToNewOffsets.Add(BitConverter.ToInt32(originalBytes, posBlockStart), BitConverter.ToInt32(originalBytes, posBlockStart));

            int stringPos = oldToNewOffsets.Keys.First();

            for (int i = 1; i < posAmount; i++) //start at 1 because we're only calculating ends, the first start is always the same
            {
                var oldOffset = BitConverter.ToInt32(originalBytes, posBlockStart + i * 4);
                if (oldToNewOffsets.ContainsKey(oldOffset))
                {
                    var bs = BitConverter.GetBytes(oldToNewOffsets[oldOffset]);
                    output[posBlockStart + i * 4] = bs[0];
                    output[posBlockStart + i * 4 + 1] = bs[1];
                    output[posBlockStart + i * 4 + 2] = bs[2];
                    output[posBlockStart + i * 4 + 3] = bs[3];
                    continue;
                }

                var line = lines[selectedLine++];
                int j = 0;
                while (j < line.Length)
                {
                    var c = line[j];
                    if (Encoder.ContainsKey(c))
                    {
                        stringPos++;
                        output.Add(Encoder[c]);
                    }
                    else
                    {
                        if (c == '{')
                        {
                            var hex = line[j + 1] + "" + line[j + 2];
                            if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + selectedLine + ", pos " + j);
                            j += 3;
                            output.Add(Helpers.CallTryParse(hex));
                            stringPos++;
                        }
                        else throw new InvalidDataException("Invalid char in line " + selectedLine + " at position " + j);
                    }
                    j++;
                }
                output.Add(0x00);
                stringPos++;

                var newOffset = stringPos;
                oldToNewOffsets[oldOffset] = newOffset;

                var bts = BitConverter.GetBytes(newOffset);
                output[posBlockStart + i * 4] = bts[0];
                output[posBlockStart + i * 4 + 1] = bts[1];
                output[posBlockStart + i * 4 + 2] = bts[2];
                output[posBlockStart + i * 4 + 3] = bts[3];
            }

            //update block size value
            var blockEnd = BitConverter.GetBytes(output.Count - blockStart);
            output[blockStart + 8] = blockEnd[0];
            output[blockStart + 9] = blockEnd[1];
            output[blockStart + 10] = blockEnd[2];
            output[blockStart + 11] = blockEnd[3];

            for (int k = 0; k < endingZeros; k++) //Append ending zeros for consistency
                output.Add(0x00);

            return output;
        }
    }
}
