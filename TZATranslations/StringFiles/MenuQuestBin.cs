﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.StringFiles
{
    /// <summary>
    /// Handles file quest01.bin, found in myoshiok/us/quest
    /// </summary>
    public class MenuQuestBin : IStringFile
    {

        const int blocksStart = 0x10;

        byte[] originalBytes;
        short blockAmount;

        public MenuQuestBin(byte[] FileBytes)
        {
            originalBytes = FileBytes;
            blockAmount = BitConverter.ToInt16(originalBytes, 0xE);
        }

        /// <summary>
        /// Gets a position offset.
        /// </summary>
        /// <param name="index">The block index.</param>
        /// <param name="type">The subfile type, between 0 and 2.</param>
        int GetPositionOffset(int index, int type) => blocksStart + 0x4 + 0x14 * index + type * 0x2;

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var lines = new List<string>();
            lines.AddRange(Decode1(Decoder));
            lines.AddRange(Decode3(Decoder));
            return lines.ToArray();
        }

        string[] Decode1(Dictionary<byte, char> Decoder)
        {
            var output = String.Empty;
            var sectionOneStart = BitConverter.ToInt16(originalBytes, 0x00);
            var sectionTwoStart = BitConverter.ToInt16(originalBytes, 0x06);
            var positions = new List<int>();
            for (int i = 0; i < blockAmount; i++)
            {
                var readPosition = BitConverter.ToInt16(originalBytes, GetPositionOffset(i, 0));
                positions.Add(sectionOneStart + readPosition);
            }
            for(int i = 0; i < positions.Count; i++)
            {
                var pos = positions[i];
                var end = i + 1 < positions.Count ? positions[i + 1] : sectionTwoStart;
                var line = String.Empty;
                byte bt;
                do
                {
                    bt = originalBytes[pos++];
                    if (Decoder.ContainsKey(bt))
                        line += Decoder[bt];
                    else
                        line += "{" + bt.ToString("X2") + "}";
                } while (pos < end);
                output += line.Substring(0, line.Length - 4) + '\n';
            }
            return output.Substring(0, output.Length - 1).Split('\n');
        }

        string[] Decode3(Dictionary<byte, char> Decoder)
        {
            var output = String.Empty;
            var sectionThreeStart = BitConverter.ToInt16(originalBytes, 0x08);
            var sectionFourStart = BitConverter.ToInt16(originalBytes, 0x0A);
            var blockPositions = new List<int>();
            for (int i = 0; i < blockAmount; i++)
            {
                var readPosition = BitConverter.ToInt16(originalBytes, GetPositionOffset(i, 2));
                blockPositions.Add(sectionThreeStart + readPosition);
            }
            for(int i = 0; i < blockPositions.Count; i++)
            {
                var init = blockPositions[i] + 0x10;

                bool isSpecial = BitConverter.ToInt32(originalBytes, blockPositions[i] + 0xC) == 0x5;

                var stringPos = new List<int>();
                int l = 0;
                if (isSpecial)
                {
                    int specialCounter = 0;
                    while (true)
                    {
                        var offset = init + 0x02 * l++;
                        if (specialCounter == 10) break;
                        stringPos.Add(BitConverter.ToInt16(originalBytes, offset));
                        specialCounter++;
                    }
                }
                else
                    while (true)
                    {
                        var offset = init + 0x04 * l++;
                        if (originalBytes[offset] != 0x00 || originalBytes[offset + 1] != 0x80) break;
                        stringPos.Add(BitConverter.ToInt16(originalBytes, offset + 0x02));
                    }

                for(int k = 0; k < stringPos.Count; k++)
                {
                    var pos = init + stringPos[k] - 0x4;
                    var line = String.Empty;
                    byte bt;
                    do
                    {
                        bt = originalBytes[pos++];
                        if (Decoder.ContainsKey(bt))
                            line += Decoder[bt];
                        else
                            line += "{" + bt.ToString("X2") + "}";
                    } while (bt != 0);
                    output += line.Substring(0, line.Length - 4) + '\n';
                }
            }
            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] NewLines)
        {
            var output = new List<byte>();

            #region SECTION ONE
            var sectionOneStart = BitConverter.ToInt16(originalBytes, 0x00); //add all of the positionals
            for (int i = 0; i < sectionOneStart; i++)
                output.Add(originalBytes[i]);

            var linesRead = 0;
            for(int i = 0; i < blockAmount; i++) //Getting hunt logs
            {
                var positionOffset = GetPositionOffset(i, 0);
                var newOffset = output.Count - sectionOneStart;
                var newOffsetBts = BitConverter.GetBytes(newOffset);
                output[positionOffset] = newOffsetBts[0];
                output[positionOffset + 1] = newOffsetBts[1];

                var line = NewLines[linesRead++];
                int j = 0;
                while (j < line.Length)
                {
                    var c = line[j];
                    if (Encoder.ContainsKey(c))
                    {
                        output.Add(Encoder[c]);
                    }
                    else
                    {
                        if (c == '{')
                        {
                            var hex = line[j + 1] + "" + line[j + 2];
                            if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + i + ", pos " + j);
                            j += 3;
                            output.Add(Helpers.CallTryParse(hex));
                        }
                        else throw new InvalidDataException("Invalid char in line " + i + " at position " + j);
                    }
                    j++;
                }
                output.Add(0x00);
            }
            var bts = BitConverter.GetBytes(output.Count);
            output[0x06] = bts[0];
            output[0x07] = bts[1];
            #endregion

            #region SECTION TWO
            var originalSectionTwoStart = BitConverter.ToInt16(originalBytes, 0x06);
            var originalSectionThreeStart = BitConverter.ToInt16(originalBytes, 0x08);

            for(int i = originalSectionTwoStart; i < originalSectionThreeStart; i++)
                output.Add(originalBytes[i]);

            bts = BitConverter.GetBytes(output.Count);
            output[0x08] = bts[0];
            output[0x09] = bts[1];
            #endregion

            #region SECTION THREE
            var sectionThreeStart = output.Count();
            var originalSectionFourStart = BitConverter.ToInt16(originalBytes, 0xA);
            for (int i = 0; i < blockAmount; i++)
            {
                var positionOffset = GetPositionOffset(i, 2);
                var newOffset = output.Count - sectionThreeStart;
                var newOffsetBts = BitConverter.GetBytes(newOffset);
                output[positionOffset] = newOffsetBts[0];
                output[positionOffset + 1] = newOffsetBts[1];

                var linesBytes = new List<byte>();
                var originalPos = BitConverter.ToInt16(originalBytes, positionOffset) + originalSectionThreeStart;
                var pos = originalPos;
                while(pos < originalPos + 0x10)
                    output.Add(originalBytes[pos++]);

                var header = BitConverter.ToInt32(originalBytes, pos - 0x4);
                bool type5 = header == 0x5;
                bool type1 = header == 0x101;
                int type5Counter = 0;

                var subBlocksRead = 0;
                while((type5 && type5Counter < 10) || (!type5 && originalBytes[pos] == 0x00 && originalBytes[pos+1] == 0x80))
                {
                    if (!type5)
                    {
                        output.Add(0x00);
                        output.Add(0x80);
                    }
                    else
                        type5Counter++;

                    var stringOffsetBts = BitConverter.GetBytes(linesBytes.Count + (type5 ? 0x18 : (type1 ? 0x8 : 0xC)));
                    output.Add(stringOffsetBts[0]);
                    output.Add(stringOffsetBts[1]);

                    var line = NewLines[linesRead++];
                    int j = 0;
                    while (j < line.Length)
                    {
                        var c = line[j];
                        if (Encoder.ContainsKey(c))
                        {
                            linesBytes.Add(Encoder[c]);
                        }
                        else
                        {
                            if (c == '{')
                            {
                                var hex = line[j + 1] + "" + line[j + 2];
                                if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + i + ", pos " + j);
                                j += 3;
                                linesBytes.Add(Helpers.CallTryParse(hex));
                            }
                            else throw new InvalidDataException("Invalid char in line " + i + " at position " + j);
                        }
                        j++;
                    }
                    linesBytes.Add(0x00);
                    subBlocksRead++;
                    pos += type5 ? 2 : 4;
                }
                output.AddRange(linesBytes);
                while(subBlocksRead > 0)
                    if (originalBytes[pos++] == 0x00)
                        subBlocksRead--;

                while (originalBytes[pos++] == 0x00 && pos < originalSectionFourStart)
                    output.Add(0x00);

                if(i + 1 < blockAmount)
                {
                    var nextPos = BitConverter.ToInt16(originalBytes, GetPositionOffset(i + 1, 2)) + originalSectionThreeStart;
                    while (pos < nextPos)
                        output.Add(originalBytes[pos++]);
                }
            }
            output.Add(0x00);
            #endregion

            #region SECTION FOUR
            bts = BitConverter.GetBytes(output.Count);
            output[0xA] = bts[0];
            output[0xB] = bts[1];

            for (int i = BitConverter.ToInt16(originalBytes, 0xA); i < originalBytes.Length; i++)
                output.Add(originalBytes[i]);
            #endregion

            return output;
        }
    }
}
