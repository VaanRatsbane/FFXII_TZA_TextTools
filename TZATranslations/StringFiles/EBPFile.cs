﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.StringFiles
{
    /// <summary>
    /// To deal with the .ebp files found in ps2data/plan_master
    /// </summary>
    public class EBPFile : IStringFile
    {
        byte[] originalBytes;
        const int filePositionsStart = 0x18;
        List<int> oldPositionsOffsets;

        int[] stringPositions;
        int stringBlockSize;
        int stringsStart;

        public EBPFile(byte[] FileBytes)
        {
            var blockSize = BitConverter.ToInt32(FileBytes, 0x10);
            int nrOfSectors = (blockSize - filePositionsStart) / 0x04;
            oldPositionsOffsets = new List<int>();

            for (int pos = filePositionsStart; pos < 0x10 + blockSize; pos+=4)
            {
                if(BitConverter.ToInt32(FileBytes, pos) != 0x00)
                    oldPositionsOffsets.Add(pos);
            }

            var stringBlockStart = BitConverter.ToInt32(FileBytes, oldPositionsOffsets[0]);
            stringBlockSize = BitConverter.ToInt16(FileBytes, stringBlockStart);
            var nrOfStrings = stringBlockSize / 4;
            stringPositions = new int[nrOfStrings];
            stringsStart = stringBlockStart + stringBlockSize;
            stringPositions[0] = 0;
            for (int p = 1; p < nrOfStrings; p++)
                stringPositions[p] = stringBlockStart + 0x04 * p;

            originalBytes = FileBytes;
        }

        public string[] Decode(Dictionary<byte, char> Decoder)
        {
            var stringBlockStart = BitConverter.ToInt32(originalBytes, oldPositionsOffsets[0]);
            var output = String.Empty;
            for (int i = 0; i < stringPositions.Length; i++)
            {
                int pos;
                pos = stringBlockStart;
                int offset = i == 0 ? stringBlockSize : BitConverter.ToInt32(originalBytes, stringPositions[i]);
                pos += offset;
                var line = String.Empty;
                byte bt;
                do
                {
                    bt = originalBytes[pos++];
                    if (Decoder.ContainsKey(bt))
                        line += Decoder[bt];
                    else
                        line += "{" + bt.ToString("X2") + "}";
                } while (bt != 0);
                output += line.Substring(0, line.Length - 4) + '\n';
            }
            return output.Substring(0, output.Length - 1).Split('\n');
        }

        public List<byte> Encode(Dictionary<char, byte> Encoder, string[] NewLines)
        {
            if (stringPositions.Length != NewLines.Length)
                throw new Exception("Line count does not match original's.");

            var stringBlockStart = BitConverter.ToInt32(originalBytes, oldPositionsOffsets[0]);
            var startOfStrings = stringBlockStart + stringBlockSize;
            var output = new List<byte>();
            for (int i = 0; i < startOfStrings; i++)
                output.Add(originalBytes[i]);

            for(int i = 0; i < NewLines.Length; i++)
            {
                var line = NewLines[i];
                int j = 0;
                while (j < line.Length)
                {
                    var c = line[j];
                    if (Encoder.ContainsKey(c))
                    {
                        output.Add(Encoder[c]);
                    }
                    else
                    {
                        if (c == '{')
                        {
                            var hex = line[j + 1] + "" + line[j + 2];
                            if (line[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex at line " + i + ", pos " + j);
                            j += 3;
                            output.Add(Helpers.CallTryParse(hex));
                        }
                        else throw new InvalidDataException("Invalid char in line " + i + " at position " + j);
                    }
                    j++;
                }
                output.Add(0x00);

                if (i + 1 < NewLines.Length) //to avoid adding offset to end
                {
                    var offsetPos = stringBlockStart + (i + 1) * 4;
                    var bts = BitConverter.GetBytes(output.Count - stringBlockStart);
                    output[offsetPos] = bts[0];
                    output[offsetPos + 1] = bts[1];
                    output[offsetPos + 2] = bts[2];
                    output[offsetPos + 3] = bts[3];
                }
            }

            var sectionOneStart = BitConverter.ToInt32(originalBytes, oldPositionsOffsets[1]);

            //add final zeros of block section
            for (int i = sectionOneStart - 2; originalBytes[i] == 0x00; i--)
                output.Add(0x00);

            var newSize1 = output.Count;
            var difference = sectionOneStart - newSize1;

            //add rest of file
            for (int i = sectionOneStart; i < originalBytes.Length; i++)
                output.Add(originalBytes[i]);
            
            for(int i = 1; i < oldPositionsOffsets.Count; i++)
            {
                var oldOffset = BitConverter.ToInt32(originalBytes, oldPositionsOffsets[i]);
                var bts = BitConverter.GetBytes(oldOffset - difference);
                output[oldPositionsOffsets[i]] = bts[0];
                output[oldPositionsOffsets[i] + 1] = bts[1];
                output[oldPositionsOffsets[i] + 2] = bts[2];
                output[oldPositionsOffsets[i] + 3] = bts[3];
            }

            return output;
        }
    }
}