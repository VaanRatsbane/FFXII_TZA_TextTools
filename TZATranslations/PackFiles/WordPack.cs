﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations.PackFiles
{
    public class WordPack : IPackFile
    {
        byte[] originalBytes;

        List<byte[]> SubFiles;

        public int SubFileCount { get { return SubFiles.Count; } }
        public byte[] GetSubFile(int index) => SubFiles[index];
        public void UpdateSubFile(int index, byte[] NewBytes) => SubFiles[index] = NewBytes;

        public WordPack(byte[] FileBytes)
        {
            originalBytes = FileBytes;

            var sectionAmount = BitConverter.ToInt32(originalBytes, 0);
            var sectionStarts = new List<int>();
            int pos = 0x04;
            while (pos < 0x04 + sectionAmount * 4)
            {
                sectionStarts.Add(BitConverter.ToInt32(originalBytes, pos));
                pos += 0x04;
            }
            int EOF = BitConverter.ToInt32(originalBytes, pos); //after the offsets, there's a final position

            SubFiles = new List<byte[]>();

            for (int processed = 0; processed < sectionAmount; processed++)
            {
                var newBytes = new List<byte>();
                var start = sectionStarts[processed];
                var end = processed + 1 < sectionAmount ? sectionStarts[processed + 1] : EOF;

                for (int i = start; i < end; i++)
                    newBytes.Add(originalBytes[i]);

                SubFiles.Add(newBytes.ToArray());
            }
        }

        public List<byte> Repack()
        {
            int posStart = 0x4;
            int posEnd = 0x44;
            var newBytes = new List<byte>();

            var positions = new List<int>();
            for (int i = posStart; i < posEnd; i += 4)
            {
                var position = BitConverter.ToInt32(originalBytes, i);
                if (!positions.Contains(position) && (positions.Count == 0 || position > positions.Last()))
                    positions.Add(position);
            }

            var oldToNew = new Dictionary<int, int>();
            for (int i = 0; i < 0x44; i++)
                newBytes.Add(originalBytes[i]);

            var filesCounter = 0;

            oldToNew.Add(positions[0], positions[0]);

            for (int i = posStart + 4; i < posEnd; i += 4)
            {
                var oldPos = BitConverter.ToInt32(originalBytes, i);
                byte[] posBytes;
                if (!(oldToNew.Count > 0 && oldToNew.ContainsKey(oldPos))) //doesn't have it, insert next file
                {
                    var fileBytes = SubFiles[filesCounter++];
                    foreach (var bt in fileBytes)
                        newBytes.Add(bt);
                    var lastPos = oldToNew.Values.Last() + fileBytes.Length;
                    posBytes = BitConverter.GetBytes(lastPos);
                    oldToNew.Add(oldPos, lastPos);
                }
                else //already has it, update pos
                {
                    posBytes = BitConverter.GetBytes(oldToNew[oldPos]);
                }
                newBytes[i] = posBytes[0];
                newBytes[i + 1] = posBytes[1];
                newBytes[i + 2] = posBytes[2];
                newBytes[i + 3] = posBytes[3];
            }

            return newBytes;
        }
    }
}
