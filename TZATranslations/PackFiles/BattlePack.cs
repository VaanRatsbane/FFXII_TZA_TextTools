﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TZATranslations.StringFiles;

namespace TZATranslations.PackFiles
{
    public class BattlePack : IPackFile
    {
        byte[] originalBytes;

        List<byte[]> SubFiles;

        public int SubFileCount { get { return SubFiles.Count; } }
        public byte[] GetSubFile(int index) => SubFiles[index];
        public void UpdateSubFile(int index, byte[] NewBytes) => SubFiles[index] = NewBytes;

        public BattlePack(byte[] FileBytes)
        {
            originalBytes = FileBytes;

            int posStart = 0x004;
            int posEnd = 0x124;

            var positions = new List<int>();
            for (int i = posStart; i < posEnd; i += 4)
            {
                var position = BitConverter.ToInt32(FileBytes, i);
                if (!positions.Contains(position))
                    positions.Add(position);
            }

            SubFiles = new List<byte[]>();

            for (var i = 0; i < positions.Count() - 1; i++)
            {
                var position = positions[i];
                int end = positions[i + 1];
                end = i + 1 == positions.Count() ? FileBytes.Length : positions[i + 1];
                var fileArray = new byte[end - position - 4];
                Array.Copy(FileBytes, position, fileArray, 0, end - position - 4); //always -4 because endings of battlepack are 4 0x00
                SubFiles.Add(fileArray);
            }
        }

        public List<byte> Repack()
        {
            int posStart = 0x004;
            int posEnd = 0x124;
            var newBytes = new List<byte>();

            var positions = new List<int>();
            for (int i = posStart; i < posEnd; i += 4)
            {
                var position = BitConverter.ToInt32(originalBytes, i);
                if (!positions.Contains(position) && (positions.Count == 0 || position > positions.Last()))
                    positions.Add(position);
            }

            var oldToNew = new Dictionary<int, int>();
            for (int i = 0; i < 0x128; i++)
                newBytes.Add(originalBytes[i]);

            var filesCounter = 0;

            oldToNew.Add(positions[0], positions[0]);
            var subfileEnding = new byte[] { 0x00, 0x00, 0x00, 0x00 };
            for (int i = posStart + 4; i < posEnd; i += 4)
            {
                var oldPos = BitConverter.ToInt32(originalBytes, i);
                byte[] posBytes;
                if (!oldToNew.ContainsKey(oldPos)) //doesn't have it, insert next file
                {
                    var fileBytes = SubFiles[filesCounter++];
                    foreach (var bt in fileBytes)
                        newBytes.Add(bt);
                    newBytes.AddRange(subfileEnding);
                    var lastPos = oldToNew.Values.Last() + fileBytes.Length + 4; //add four after adding subfile ending 4 x 0x00
                    posBytes = BitConverter.GetBytes(lastPos);
                    oldToNew.Add(oldPos, lastPos);
                }
                else //already has it, update pos
                {
                    posBytes = BitConverter.GetBytes(oldToNew[oldPos]);
                }
                newBytes[i] = posBytes[0];
                newBytes[i + 1] = posBytes[1];
                newBytes[i + 2] = posBytes[2];
                newBytes[i + 3] = posBytes[3];
            }
            //Append 4 zeros at the end
            newBytes.AddRange(subfileEnding);
            return newBytes;
        }
    }
}
