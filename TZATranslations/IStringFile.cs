﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations
{
    public interface IStringFile
    {
        string[] Decode(Dictionary<byte, char> Decoder);
        List<byte> Encode(Dictionary<char, byte> Encoder, string[] NewLines);
    }
}
