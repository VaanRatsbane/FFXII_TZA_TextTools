﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations
{
    public interface IPackFile
    {
        List<byte> Repack();
        byte[] GetSubFile(int index);
        void UpdateSubFile(int index, byte[] NewBytes);
    }
}
