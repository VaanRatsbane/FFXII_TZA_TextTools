﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TZATranslations
{
    /// <summary>
    /// Translation Class for TZA text files.
    /// </summary>
    public class Studio
    {

        private Dictionary<byte, char> byteToChar;
        private Dictionary<char, byte> charToByte;

        /// <summary>
        /// Creates a new translation studio.
        /// </summary>
        /// <param name="Dictionary">Byte to char encoding.</param>
        public Studio(Dictionary<byte, char> Dictionary)
        {
            byteToChar = Dictionary;
            charToByte = new Dictionary<char, byte>();

            foreach (var pair in Dictionary)
                charToByte.Add(pair.Value, pair.Key);
            charToByte.Add('\n', 0x00); //decoder should not have 0x00
        }

        /// <summary>
        /// Decodes a byte array into a string.
        /// </summary>
        /// <param name="bytes">The bytes to decode.</param>
        /// <returns>The decoded string.</returns>
        public string Decode(byte[] bytes)
        {
            var str = String.Empty;
            foreach(var bt in bytes)
            {
                if (byteToChar.ContainsKey(bt))
                    str += byteToChar[bt];
                else
                    str += "{" + bt.ToString("X2") + "}";
            }
            return str;
        }

        /// <summary>
        /// Encodes a string into a byte array.
        /// </summary>
        /// <param name="text">The string to encode.</param>
        /// <returns>The encoded array.</returns>
        public byte[] Encode(string text)
        {
            var output = new List<byte>();
            int j = 0;
            while (j < text.Length)
            {
                var c = text[j];
                if (charToByte.ContainsKey(c))
                {
                    output.Add(charToByte[c]);
                }
                else
                {
                    if (c == '{')
                    {
                        var hex = text[j + 1] + "" + text[j + 2];
                        if (text[j + 3] != '}') throw new InvalidDataException("Error encapsulating hex pos " + j);
                        j += 3;
                        output.Add(Helpers.CallTryParse(hex));
                    }
                    else throw new InvalidDataException("Invalid char in position " + j);
                }
            }
            return output.ToArray();
        }

        /// <summary>
        /// Decodes a loaded file into string lines.
        /// </summary>
        /// <param name="file">The loaded bin file.</param>
        /// <returns>The decoded string lines.</returns>
        public string[] DecodeFile(IStringFile file) => file.Decode(byteToChar);

        /// <summary>
        /// Encodes the file with new strings.
        /// </summary>
        /// <param name="file">The original loaded bin file.</param>
        /// <param name="newLines">The new string lines.</param>
        /// <returns>The encoded file's new bytes.</returns>
        public List<byte> EncodeBin(IStringFile file, string[] newLines) => file.Encode(charToByte, newLines);
    }
}
